/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//Authors: Christine Gleerup-Morch and Petr Vaněk
//Applied Operations Research course at KU
//2019

package benders;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;


public class OptimalitySubProblem {
    
    //We define the optimallity subproblem, its decitions varibles and its constrains: 
    
    private final IloCplex model;
    private final IloNumVar l[];
    private final IloNumVar p[][];
    private final UnitCommitmentProblem ucp;
    private final IloRange Constraints1e[];
    private final IloRange Constraints1f[][];
    private final IloRange Constraints1g[][];
    private final IloRange Constraints1h[][];
    private final IloRange Constraints1i[][];
    
    
    /**
     // We create the optimallity subproblem
     * @param ucp
     * @param U a solution to MP
     * @throws IloException 
     */
    
    public OptimalitySubProblem(UnitCommitmentProblem ucp, double U[][]) throws IloException{
        this.ucp = ucp;
        
        // The model need a IloCplex object:
        this.model = new IloCplex();
        
        // We create the decision variables
        
        
        // l_t
        l = new IloNumVar[ucp.getnHours()];
        for(int t = 1; t <= ucp.getnHours(); t++){
            l[t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
        }
        
        // p_gt
        p = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                p[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }
        }
        
        // We create the objectfunction
        
        IloLinearNumExpr obj = model.linearNumExpr();
        
        
        // To the objective function we add the decitions varibles. 

        for(int t = 1; t<= ucp.getnHours(); t++){
            obj.addTerm(ucp.getSheddingCost(t),l[t-1]);
            for(int g =1; g <= ucp.getnGenerators(); g++){
                obj.addTerm(ucp.getProductionCost(g),p[g-1][t-1]);
            }
        }
        
        // We tell cplex to minimize the objective function

        model.addMinimize(obj);
        
        // Now we creates the constraints for the Optimality subproblem
        
            
            // Constraints (1e)
            this.Constraints1e = new IloRange[ucp.getnHours()];
            for(int t = 1; t <= ucp.getnHours(); t++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                lhs.addTerm(1, p[g-1][t-1]);
            }
            lhs.addTerm(1, l[t-1]);
            Constraints1e[t-1] = model.addEq(lhs,ucp.getDemand(t));
        }
            
             // Constraints (1f)
            this.Constraints1f = new IloRange[ucp.getnGenerators()][ucp.getnHours()];     
            for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                Constraints1f[g-1][t-1] = model.addGe(lhs,ucp.getMinProduction(g)*U[g-1][t-1]);
            }
        }
            
            //Constraints (1g)
            this.Constraints1g = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
            for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                Constraints1g[g-1][t-1] = model.addLe(lhs,ucp.getMaxProduction(g)*U[g-1][t-1]);
            }
        }    
        
            // Constraints (1h)
            this.Constraints1h = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
            for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                if(t > 1){
                    lhs.addTerm(-1,p[g-1][t-2]); // Note that this value can only be created if t>1 due to the index t-2 which correspond to t-1 when not using code. 
                }
                Constraints1h[g-1][t-1] = model.addLe(lhs, ucp.getRampUp(g));
            }
        }
        
        
        // Constraints (1i)
            this.Constraints1i = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
            for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                if(t > 1){
                    lhs.addTerm(1,p[g-1][t-2]);  // Note that this value can only be created if t>1 due to the index t-2 which correspond to t-1 when not using code. 
                }
                lhs.addTerm(-1,p[g-1][t-1]);
                Constraints1i[g-1][t-1] = model.addLe(lhs, ucp.getRampDown(g));
            }
        }
    }

    
    
    // We solve the model:
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
    }
    
    // We make a get Objective to have a way to acces the objective value of the optimality subproblem:
    
    public double getObjective() throws IloException{
        return model.getObjValue();
    }
    
    // Now we create the optimallyty cut. This cut is used when we allredy have a solution which is
    // feasible. Inside the callback in the masterproblem we check for optimality. If our current 
    // solution don't forfill the optimality check then we need to add a optimality cut to our
    // master problem. The optimality cut is created below.
    
    // We create the constant cut term for the optimality cut. 
    // To the constant cut term we are adding all the constant terms form the above constrains (the constants that are not depending on u). 
    // These terms are mutiplied with the dual varibles form this subproblem.
    
    public double getCutConstant() throws IloException{
        double constant = 0;
        for(int t = 1; t <= ucp.getnHours(); t++){
		constant = constant + model.getDual(Constraints1e[t-1]) * ucp.getDemand(t);
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                constant = constant + model.getDual(Constraints1h[g-1][t-1]) * ucp.getRampUp(g) + model.getDual(Constraints1i[g-1][t-1]) * ucp.getRampDown(g);
            }
        }
        return constant;
    }
    
    // We are creating the linear term which dependt on all the terms form the  above constrains that 
    // dependt on u. Furthermore, these terms are multipled by the dual varible for this subproblem.
    
    public IloLinearNumExpr getCutLinearTerm(IloNumVar[][] u) throws IloException {
        IloLinearNumExpr cutTerm = model.linearNumExpr();
        for(int g = 1; g<= ucp.getnGenerators(); g++){
            for(int t = 1; t<= ucp.getnHours(); t++){
                cutTerm.addTerm(model.getDual(Constraints1f[g-1][t-1])*ucp.getMinProduction(g), u[g-1][t-1]);
                cutTerm.addTerm(model.getDual(Constraints1g[g-1][t-1])*ucp.getMaxProduction(g), u[g-1][t-1]);
            }
        }
        return cutTerm;
    }
    
    // We release all the objects form IloCplex. 
    public void end(){
        model.end();
    } 
   
}
