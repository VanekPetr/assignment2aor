/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 
//Authors: Christine Gleerup-Morch and Petr Vaněk
//Applied Operations Research course at KU
//2019
package benders;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

public class MasterProblem {
    
    // As a part of the bender decomposition we are creating the master problem. The master problem 
    // is the problem that will contain the complicated varibles of the original problem. In this problem 
    // we are finding the varible u_gt complicated becouse it is a bineary varible. Furthermore, we are
    // also including c_gt in the master problem. The master problem will allso contain the the feasibility 
    // and optimality cuts which are added when going through the algorithm.  
    
    // We are defining which elements we are going to have in our masterproblem. 
    
    private final IloCplex model;
    private final IloNumVar c[][];
    private final IloIntVar u[][];
    private final IloNumVar phi;
    private final UnitCommitmentProblem ucp;
    
    // We are now creating the masterproblem 
    //@param ucp
    //@throws IloException
    
    public MasterProblem(UnitCommitmentProblem ucp) throws IloException{
        this.ucp = ucp;
        
        // Our model needs a IloCplex object:
        this.model = new IloCplex();  
        
        // Creates the decision variables 
        
        u_gt
        this.u = new IloIntVar[ucp.getnGenerators()][ucp.getnHours()];
        // Assigns a value for each position in the array:
        for(int g = 1; g<= ucp.getnGenerators(); g++){
            for( int t = 1; t<= ucp.getnHours(); t++ ){
                u[g-1][t-1] = model.boolVar();
            } 
        }

	
	// Creates c_gt

	    this.c = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        // Assigns a value for each position in the array:
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){
                c[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
            }
        }
        
        // We define the other part of the object function for the master problem which is phi. 
        //Phi contains the rest of the objective function form the original problem. 
        
        this.phi = model.numVar(0, Double.POSITIVE_INFINITY,"phi");
        
        
        // We create the object function: 
        IloLinearNumExpr obj = model.linearNumExpr();
        
        // Adds terms to the objectfunction: 
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t <= ucp.getnHours(); t++){ 
		obj.addTerm(1,c[g-1][t-1]);
                obj.addTerm(ucp.getCommitmentCost(g), u[g-1][t-1]);
            }
        }
        // Adds phi to the object function:
        obj.addTerm(1, phi);
        
        // We want to minimize the objective function of the master problem:
        model.addMinimize(obj);
    
        // Creates the constraints

	//Constrain (1b)
	//The as the one form UnitCommitmentModel
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, c[g-1][t-1]);
                // chinging the right hand side
                lhs.addTerm(-ucp.getStartUpCost(g), u[g-1][t-1]);
                if(t > 1){
                    lhs.addTerm(+ucp.getStartUpCost(g), u[g-1][t-2]); // t has to be largere than 1 to get the value u[g-1][t-2] which is the code for u_{g,t-1}              
                }
                model.addGe(lhs, 0);
            }
        }


        
        // Constraints (1c)
        //The as the one form UnitCommitmentModel
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                // We loop over t'
                for(int tt = t; tt <= ucp.getMinOnTimeAtT(g, t); tt++){
                    lhs.addTerm(1, u[g-1][tt-1]);
                    lhs.addTerm(-1, u[g-1][t-1]);
                    if(t > 1){
                        lhs.addTerm(1, u[g-1][t-2]);
                    }
                }
                model.addGe(lhs,0);
            }
        }
        
        // Constraints (1d)
        //The as the one form UnitCommitmentModel
        for(int t = 1; t <= ucp.getnHours(); t++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                // We loop over t'
                // We add one every time we loop. We save this sum in a constant
                int constant = 0;
                for(int tt = t; tt <= ucp.getMinOffTimeAtT(g, t); tt++){
                    constant++;
                    lhs.addTerm(-1, u[g-1][tt-1]);
                    lhs.addTerm(1, u[g-1][t-1]);
                    if(t > 1){
                        lhs.addTerm(-1, u[g-1][t-2]);
                    }
                }
                model.addGe(lhs,constant);
            }
        }
        
    }
    

    // We solve the master problem
    // @throws IloException 
     
    public void solve() throws IloException{
        
        // When solving the problem we are going to use Callback. The Callback is create below:
        model.use(new MyCallback());
        
        model.solve();
    }

    
    
    // Defining the callback used for solving the problem. The actions inside the callback will be
    // runed through every time we reach an integer node:
    
    private class MyCallback extends IloCplex.LazyConstraintCallback{
        
        public MyCallback() {
        }
    
        @Override
        protected void main() throws IloException {
            // The solution to U and phi at every intger node:
            double[][] U = getU();
            double Phi = getPhi();
            
            // For the feasibility subproblem
            
            // Creating and solving a feasibility subproblem 
            FeasibilitySubProblem fsp = new FeasibilitySubProblem(ucp,U);
            fsp.solve();
            double fspObjective = fsp.getObjective();
            
            // We check if the subproblem problem is feasible which is when the objective value is 0.
            System.out.println("FSP "+fspObjective);
            if(fspObjective >= 0+1e-9){
           
                // If the objective is not 0 then the subproblem is not feasible and 
                // we need to add the feasibility cut to the master problem

                System.out.println("Generating feasibility cut");
                       
                // Getting the constant and linear term form the feasibility subproblem
               
                double constantTerm = fsp.getCutConstant();
                IloLinearNumExpr linearTerm = fsp.getCutLinearTerm(u);
                
                
                // Now we are putting together the feasibility cut  linearTerm <= -constant
                // and add the cut to the problem
                
                add(model.le(linearTerm, -constantTerm));

            }else{
                
                // In this case the subproblem is feasible and therefore we don't add a feasiblity cut
                // instead we are continuing and cheacking for optimality 
                
                // Creating and solving the optimality subproblem:
                OptimalitySubProblem osp = new OptimalitySubProblem(ucp,U);
                osp.solve();
                double ospObjective = osp.getObjective();
                
                //  We test if the solution is optimal:
                System.out.println("Phi "+Phi+ " OSP "+ospObjective );
                if(Phi >= ospObjective - 1e-9){
                    // If this statement is forfilled the current node is optimal and we don't add the optimality cut. 

                    System.out.println("The current node is optimal");
                }else{
                    
                    // In this case the current node is not optimal and therefore we need to add
                    // an optimality cut.
 
                    System.out.println("Generating optimality cut");
                    
                    // Getting the constant and linear term form the optimallity subproblem
                    
                    double cutConstantTerm = osp.getCutConstant();
                    IloLinearNumExpr cutTerm = osp.getCutLinearTerm(u);
                   
                    // Now we can create the optimality cut which is -phi * cutTerm <= -cutConstantTerm
                    // and then we are adding this cut to the model.
                    
                    cutTerm.addTerm(-1, phi);
                    // and generate and add a cut. 
                    add(model.le(cutTerm, -cutConstantTerm));
                    
                }
            }
        }
    
    

        // @return the value of U form the current solution
        // @throws IloException 
        
        public double[][] getU() throws IloException{
           double U[][] = new double[ucp.getnGenerators()][ucp.getnHours()];
           for(int g = 1; g<= ucp.getnGenerators() ;g++){
               for(int t = 1; t<= ucp.getnHours(); t++){
                   U[g-1][t-1] = getValue(u[g-1][t-1]);
               }
           }  
           return U;
       }
       
        // Create a metode to get the Phi 
        
        public double getPhi() throws IloException{
           return getValue(phi);
        }
    } 
        
        
        
     // Create a metode to get the objective value  
     
        public double getObjective() throws IloException{
        return model.getObjValue();
    }
       
    // We print the value for each U, this value should end up being either 0 or 1 because it's a binary varible
    
        public void printSolution() throws IloException{
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int t = 1; t<=ucp.getnHours(); t++){
                System.out.println("U_"+g+""+t+" = "+model.getValue((u[g-1][t-1])));
            }
        }
    }
        
        
        
        public void end(){
        model.end();
    }
        
       
}
  
